# Norwegian Bokmal translation for ubuntu-touch-tweak-tool
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the ubuntu-touch-tweak-tool package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-touch-tweak-tool\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-08 23:29+0000\n"
"PO-Revision-Date: 2020-11-10 00:20+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/"
"ut-tweak-tool/ut-tweak-tool/nb_NO/>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.4-dev\n"
"X-Launchpad-Export-Date: 2016-12-09 05:45+0000\n"

#: ../src/app/qml/aboutTab/AboutPage.qml:11
#: ../src/app/qml/aboutTab/AboutPage.qml:23
msgid "About"
msgstr "Om"

#: ../src/app/qml/aboutTab/AboutPage.qml:26
msgid "Credits"
msgstr "Bidragsytere"

#: ../src/app/qml/aboutTab/AboutPage.qml:42
#: ../src/app/qml/aboutTab/AboutPage.qml:43
#: ../src/app/qml/aboutTab/AboutPage.qml:44
msgid "Resources"
msgstr "Ressurser"

#: ../src/app/qml/aboutTab/AboutPage.qml:42
msgid "Bugs"
msgstr "Kjente feil"

#: ../src/app/qml/aboutTab/AboutPage.qml:43
msgid "Contact"
msgstr "Kontakt"

#: ../src/app/qml/aboutTab/AboutPage.qml:44
msgid "Translations"
msgstr "Oversettelser"

#: ../src/app/qml/aboutTab/AboutPage.qml:47
#: ../src/app/qml/aboutTab/AboutPage.qml:48
#: ../src/app/qml/aboutTab/AboutPage.qml:49
#: ../src/app/qml/aboutTab/AboutPage.qml:50
#: ../src/app/qml/aboutTab/AboutPage.qml:51
#: ../src/app/qml/aboutTab/AboutPage.qml:52
msgid "Developers"
msgstr "Utviklere"

#: ../src/app/qml/aboutTab/AboutPage.qml:47
msgid "Founder"
msgstr "Opphavsmann"

#: ../src/app/qml/aboutTab/AboutPage.qml:48
#: ../src/app/qml/aboutTab/AboutPage.qml:52
msgid "Maintainer"
msgstr "Vedlikeholder"

#: ../src/app/qml/aboutTab/AboutPage.qml:49
#: ../src/app/qml/aboutTab/AboutPage.qml:50
#: ../src/app/qml/aboutTab/AboutPage.qml:51
msgid "Contributor"
msgstr "Bidragsyter"

#: ../src/app/qml/aboutTab/AboutPage.qml:103
#, fuzzy
msgid "Ubuntu Touch Tweak Tool"
msgstr "Ubuntu Touch-tilpasningsverktøy"

#: ../src/app/qml/aboutTab/AboutPage.qml:108
#, qt-format
msgid "Version %1"
msgstr "Versjon %1"

#: ../src/app/qml/aboutTab/AboutPage.qml:116
msgid ""
"A Tweak tool application for Ubuntu Touch.<br/>Tweak your device, unlock its "
"full power! Advanced settings for your ubuntu device."
msgstr "En app for tilpasning av Ubuntu Touch.<br/>Tilpass din enhet og få tilgang til dens fulle potensiale! Avanserte innstillinger for din Ubuntu-enhet."

#: ../src/app/qml/aboutTab/AboutPage.qml:130
msgid ""
"A special thanks to all translators, beta-testers, and users<br/>in general "
"who improve this app with their work and feedback<br/>"
msgstr "En spesiell takk til alle oversettere, beta-testere og brukere<br/>generelt, som bidrar med arbeid og tilbakemeldinger for å bedre denne appen."

#: ../src/app/qml/aboutTab/AboutPage.qml:137
msgid ""
"(C) 2014-2016 Stefano Verzegnassi<br/>Maintainer (C) 2018 Rudi Timmermans<br/"
">"
msgstr "© 2014–2016 Stefano Verzegnassi<br/>Vedlikeholder © 2018 Rudi Timmermans<br/"
">"

#: ../src/app/qml/aboutTab/AboutPage.qml:138
msgid "Maintainer (C) 2018-2022 Imran Iqbal<br/>"
msgstr "Vedlikeholder © 2018-2022 Imran Iqbal<br/>"

#: ../src/app/qml/aboutTab/AboutPage.qml:146
msgid "Released under the terms of the GNU GPL v3"
msgstr "Utgitt i henhold til betingelsene i GNU GPL v3"

#: ../src/app/qml/aboutTab/AboutPage.qml:156
#, qt-format
msgid "Source code available on %1"
msgstr "Kildekode tilgjengelig på %1"

#: ../src/app/qml/applicationsTab/ApplicationPage.qml:14
msgid "Details"
msgstr "Detaljer"

#: ../src/app/qml/applicationsTab/ApplicationPage.qml:18
#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:41
#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:40
msgid "General"
msgstr "Generelt"

#: ../src/app/qml/applicationsTab/ApplicationPage.qml:18
msgid "Hooks"
msgstr "Koblinger"

#: ../src/app/qml/applicationsTab/ApplicationPage.qml:22
msgid "Uninstall"
msgstr "Avinstaller"

#: ../src/app/qml/applicationsTab/ApplicationPage.qml:88
msgid "Do you want to uninstall this package?"
msgstr "Vil du avinstallere pakken?"

#: ../src/app/qml/applicationsTab/ApplicationPage.qml:95
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:217
#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:165
#: ../src/app/qml/behaviourTab/Theme.qml:114
#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:296
#: ../src/app/qml/components/Dialogs/FileChooserDialog.qml:46
#: ../src/app/qml/systemTab/Services.qml:75
#: ../src/app/qml/systemTab/UsbMode.qml:111
#: ../src/plugin/pamauthentication/AuthenticationDialog.qml:62
msgid "Cancel"
msgstr "Avbryt"

#: ../src/app/qml/applicationsTab/ApplicationPage.qml:101
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:222
#: ../src/app/qml/behaviourTab/Audio.qml:208
#: ../src/app/qml/systemTab/UsbMode.qml:117
msgid "OK"
msgstr "OK"

#. TRANSLATORS: The template is defined in the AppArmor manifest of a package.
#. "<i>generic</i>" is used when no template has been defined in the manifest (i.e. it's a standard confined app).
#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:80
#, qt-format
msgid "Template: %1"
msgstr "Mal: %1"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:80
msgid "<i>generic</i>"
msgstr "<i>generisk</i>"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:81
#, qt-format
msgid "Policy version: %1"
msgstr "Ansvarsfraskrivelsesversjon: %1"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:101
msgid "Can integrate with your online accounts"
msgstr "Kan integreres med din nettkonto"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:104
msgid "Can play audio"
msgstr "Kan spille av lyd"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:107
msgid "Can access the calendar (reserved)"
msgstr "Tilgang til kalenderen (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:110
msgid "Can access the camera"
msgstr "Har tilgang til kameraet"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:113
msgid "Can access network connectivity informations"
msgstr "Har tilgang til info om nettverksforbindelse"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:116
msgid "Can access contacts"
msgstr "Har tilgang til kontakter"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:119
msgid "Can request/import data from other applications"
msgstr "Kan be om/importere data fra andre apper"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:122
msgid "Can provide/export data to other applications"
msgstr "Kan gi/eksportere data til andre apper"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:125
msgid "Use special debugging tools (reserved)"
msgstr "Bruk eksplisitte feilsøkingsverktøy (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:128
msgid "Can access message and call history (reserved)"
msgstr "Har tilgang til beskjeder og ringehistorie (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:131
msgid "In-app purchases"
msgstr "Kjøp i app"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:134
msgid "Can request keeping the screen on"
msgstr "Kan tvinge skjermen til forbli påslått"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:137
msgid "Can access to your current location"
msgstr "Har tilgang til din posisjon"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:140
msgid "Can access the microphone"
msgstr "Har tilgang til mikrofonen"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:143
msgid "Can read and write to music files (reserved)"
msgstr "Kan lese og skrive lydfiler (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:146
msgid "Can read all music files (reserved)"
msgstr "Kan lese alle typer lydfiler (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:149
msgid "Can access the network"
msgstr "Har tilgang til nettverket"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:152
msgid "Can read and write to pictures files (reserved)"
msgstr "Kan lese og skrive bildefiler (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:155
msgid "Can read all pictures files (reserved)"
msgstr "Kan lese alle bildefiler (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:158
msgid "Can use push notifications"
msgstr "Kan bruke push-varsler"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:161
msgid "Can access the device sensors"
msgstr "Har tilgang til enhetens sensorer"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:164
msgid "Can show informations on the lock screen"
msgstr "Kan vise info på låseskjermen"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:167
msgid "Can play video"
msgstr "Kan spille av video"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:170
msgid "Can read and write to video files (reserved)"
msgstr "Kan lese og skrive videofiler (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:173
msgid "Can read all video files (reserved)"
msgstr "Kan lese alle videofiler (reservert)"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:176
msgid "Can use the Ubuntu webview component"
msgstr "Kan bruke Ubuntu webview-komponenten"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:286
msgid "Can read the following additional paths:"
msgstr "Kan lese følgende stier:"

#: ../src/app/qml/applicationsTab/ApplicationPageHooksTab.qml:309
msgid "Can write to the following additional paths:"
msgstr "Kan skrive til følgende stier:"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:101
msgid "Description"
msgstr "Beskrivelse"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:115
msgid "Prevent app suspension"
msgstr "Motvirk appavbrudd"

#. TRANSLATORS: "—" is a special char (please copy-paste it into your translation) . "%1" is a formatted string (e.g. 2.56 MB)
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:138
#, qt-format
msgid "Disk usage — %1"
msgstr "Diskbruk — %1"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:144
msgid "Application size"
msgstr "Appstørrelse"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:150
msgid "Cache size"
msgstr "Mellomlagerstørrelse"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:156
msgid "Data size"
msgstr "Datastørrelse"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:162
msgid "Config size"
msgstr "Oppsettsstørrelse"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:169
msgid "Clear..."
msgstr "Slett…"

#. TRANSLATORS: %1 is the name of the application
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:175
#, qt-format
msgid "Clear %1 data"
msgstr "Slett data for %1…"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:199
msgid "Cache:"
msgstr "Hurtiglager:"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:204
msgid "Application data:"
msgstr "App-data:"

#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:209
msgid "Config:"
msgstr "Oppsett:"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:241
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:246
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:251
#, qt-format
msgid "%1 kB"
msgstr "%1 kB"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: ../src/app/qml/applicationsTab/ApplicationPageOverviewTab.qml:254
#, qt-format
msgid "%1 byte"
msgstr "%1 byte"

#: ../src/app/qml/applicationsTab/ApplicationsTab.qml:33
msgid "Show all"
msgstr "Vis alle"

#: ../src/app/qml/applicationsTab/ApplicationsTab.qml:33
msgid "Contains an app"
msgstr "Inneholder en app"

#: ../src/app/qml/applicationsTab/ApplicationsTab.qml:33
msgid "Contains a scope"
msgstr "Inneholder et applikasjonsvirkefelt"

#: ../src/app/qml/applicationsTab/ApplicationsTab.qml:39
msgid "No installed package"
msgstr "Ingen installert pakke"

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:49
msgid ""
"Set your favorite apps to be shown in the applications scope.<br>Swipe to "
"right to delete an item. Press and hold will activate the sort mode."
msgstr ""
"Set din favorittapp for visning i appvirkefeltet.<br>Dra mot høyre "
"for å slette en enhet. «Trykk og hold» aktiverer sorteringsmodus."

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:53
#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:146
#: ../src/app/qml/behaviourTab/BehaviourTab.qml:36
msgid "Favorite apps"
msgstr "Favorittapper"

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:63
msgid "No favorite specified"
msgstr "Ingen favoritter angitt"

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:64
msgid ""
"Default applications will be shown in the 'App' scope.\n"
"Tap the '+' in the header to add a favorite app."
msgstr ""
"De forvalgte appene vil vises i «App»-virkefeltet.\n"
"Trykk «+» øverst for å legge til en favorittapp."

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:83
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:67
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:118
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:170
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:227
#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:96
msgid "Reset"
msgstr "Tilbakestill"

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:93
msgid "<i>This app is currently uninstalled.</i>"
msgstr "<i>Denne appen avsinstalleres nå.</i>"

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:102
msgid "Delete"
msgstr "Slett"

#: ../src/app/qml/behaviourTab/AppsScopeFavs.qml:151
#: ../src/app/qml/behaviourTab/AppsScopeFavsAddFromInstalled.qml:34
msgid "Add new favorite"
msgstr "Legg til ny favoritt"

#: ../src/app/qml/behaviourTab/AppsScopeFavsAddFromInstalled.qml:52
msgid "Available apps"
msgstr "Tilgjengelige apper"

#: ../src/app/qml/behaviourTab/Audio.qml:46
msgid "Notification sound"
msgstr "Varsellyd"

#: ../src/app/qml/behaviourTab/Audio.qml:97
msgid ""
"Please note that if you choose an audio file from an external media (e.g. SD "
"card), that file could not be played if the media will be removed."
msgstr "Legg merke til at dersom du velger en lydfil fra eksterne "
"medier (f.eks. SD-kort) vil filen ikke kunne avspilles dersom mediet fjernes."

#: ../src/app/qml/behaviourTab/Audio.qml:100
msgid "Message received"
msgstr "Melding mottatt"

#: ../src/app/qml/behaviourTab/Audio.qml:104
msgid ""
"Ensure your audio file length is max. 10 seconds.<br><b>N.B.</b> The file "
"will be played until its end: if you use a 3 minutes song, your device will "
"be ringing for that time!"
msgstr "Lydfilen må ikke overstige 10 sekunder.<br><b>Merk</b>: Hele filen "
"blir avspilt! En tre min. lang fil vil medføre at enheten ringer så lenge."

#: ../src/app/qml/behaviourTab/Audio.qml:108
msgid ""
"This feature needs to create a file inside your system (/usr/share/sounds/"
"ubuntu/notifications) to avoid breaking notification sound from other "
"applications such as Telegram and FluffyChat.<br><b>N.B.</b> Please, note "
"that it will be removed after every system update, you'll need to set it up "
"again."
msgstr "Denne funksjonen må opprette en fil inne i systemet ditt i "
"(/user/share/sounds/ubuntu/notifications) for unngå å ødelegge merknadslyder "
"fra andre apper som Telegram og FluffyChat.<br><b>.Merk</b>: Den vil bli "
"fjernet etter hver systemoppdatering, så du må sette den opp igjen etterpå"

#: ../src/app/qml/behaviourTab/Audio.qml:114
#, fuzzy
msgid "Current notification sound:"
msgstr "Valgt varslingslyd:"

#: ../src/app/qml/behaviourTab/Audio.qml:116
#: ../src/app/qml/components/Dialogs/FileChooserDialog.qml:134
#, fuzzy
msgid "Pick"
msgstr "Velg…"

#: ../src/app/qml/behaviourTab/Audio.qml:125
#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:258
msgid "Restore default..."
msgstr "Gjenopprett forvalg"

#: ../src/app/qml/behaviourTab/Audio.qml:152
#, fuzzy
msgid "Choose a notification sound"
msgstr "Velg varslingslyd"

#: ../src/app/qml/behaviourTab/Audio.qml:176
msgid "Restore default"
msgstr "Gjenopprett forvalg"

#: ../src/app/qml/behaviourTab/Audio.qml:177
msgid "Are you sure?"
msgstr "Er du sikker?"

#: ../src/app/qml/behaviourTab/Audio.qml:180
msgid "Yes"
msgstr "Ja"

#: ../src/app/qml/behaviourTab/Audio.qml:189
msgid "No"
msgstr "Nei"

#: ../src/app/qml/behaviourTab/Audio.qml:204
msgid "MP3 File Selected"
msgstr "MP3-fil valgt"

#: ../src/app/qml/behaviourTab/Audio.qml:205
msgid ""
"OGG format is highly recommended. Currently, MP3 format breaks the "
"notification sound for other apps such as Telegram and FluffyChat"
msgstr "OGG-format er sterkt anbefalt. MP3-formatet ødelegger varslingslyden "
"for noen apper som Telegram og FluffyChat"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:32
msgid "Application scope"
msgstr "Appvirkefelt"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:43
msgid "Unity 8"
msgstr "Unity 8"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:47
#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:32
msgid "Scaling"
msgstr "Skalering"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:52
#: ../src/app/qml/behaviourTab/Unity8Mode.qml:29
#: ../src/app/qml/behaviourTab/Unity8Mode.qml:68
msgid "Usage mode"
msgstr "Brukermodus"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:57
#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:29
msgid "Launcher"
msgstr "Oppstarter"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:62
#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:29
msgid "Indicators"
msgstr "Indikatorer"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:67
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:31
msgid "Edge sensitivity"
msgstr "Kantfølsomhet"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:74
msgid "Audio"
msgstr "Lyd"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:78
#, fuzzy
msgid "Set a custom notification sound"
msgstr "Bruk en selvvalgt varslingslyd"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:85
msgid "Experimental"
msgstr "Eksperimentelt"

#: ../src/app/qml/behaviourTab/BehaviourTab.qml:89
#: ../src/app/qml/behaviourTab/Theme.qml:32
#, fuzzy
msgid "System theme"
msgstr "Systemdrakt"

#: ../src/app/qml/behaviourTab/Theme.qml:52
msgid "Unity 8 has three available themes"
msgstr "Det er tre tilgjengelige drakter i Unity 8"

#: ../src/app/qml/behaviourTab/Theme.qml:53
msgid "Currently, only the default theme (Ambiance) is well-supported."
msgstr "For tiden tilbys bra støtte kun til forvalgt drakt (Ambiance)"

#: ../src/app/qml/behaviourTab/Theme.qml:54
msgid "The other themes are only useful for testing purposes."
msgstr "Øvrige drakter benyttes til testing"

#: ../src/app/qml/behaviourTab/Theme.qml:57
msgid "Theme selection"
msgstr "Valg av drakt"

#: ../src/app/qml/behaviourTab/Theme.qml:62
msgid "(default theme)"
msgstr "(forvalgt drakt)"

#: ../src/app/qml/behaviourTab/Theme.qml:107
#: ../src/app/qml/behaviourTab/Theme.qml:122
#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:291
#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:301
#: ../src/app/qml/systemTab/Services.qml:46
#: ../src/app/qml/systemTab/Services.qml:69
#: ../src/app/qml/systemTab/Services.qml:80
#, fuzzy
msgid "Restart Unity 8"
msgstr "Start Unity 8 på nytt"

#: ../src/app/qml/behaviourTab/Theme.qml:108
msgid "This change requires restarting Unity 8 to take effect."
msgstr "Unity 8 må startes på nytt for at endringen skal tre i kraft"

#: ../src/app/qml/behaviourTab/Theme.qml:109
#: ../src/app/qml/systemTab/Services.qml:70
#, fuzzy
msgid "Do you want to restart Unity 8 now?"
msgstr "Vil ta en omstart av Unity 8 nå?"

#: ../src/app/qml/behaviourTab/Theme.qml:110
#: ../src/app/qml/systemTab/Services.qml:71
msgid "All currently open apps will be closed."
msgstr "Alle åpne apper vil bli lukket."

#: ../src/app/qml/behaviourTab/Theme.qml:111
#: ../src/app/qml/systemTab/Services.qml:72
msgid "Save all your work before continuing!"
msgstr "Lagre alt arbeidet ditt før du fortsetter!"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:42
msgid "Edge barrier sensitivity"
msgstr "Kantgrensefølsomhet"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:50
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:102
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:154
#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:209
#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:80
#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:191
#, qt-format
msgid "Current value: %1"
msgstr "Gjeldende verdi: %1"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:51
msgid "Sensitivity of screen edge barriers for the mouse pointer."
msgstr "Skjermkantgrensens følsomhet for musepeker."

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:52
msgid ""
"Some UI actions like revealing the launcher or the applications spread are "
"triggered by pushing the mouse pointer against a screen edge. This key "
"defines how much you have to push in order to trigger the associated action."
msgstr ""
"Noen brukergrensesnitthandlinger som å vise oppstarteren eller apper "
"trigges ved å trykke musepekeren mot skjermkanten. Dette definerer hvor mye "
"du må trykke for å aktivere den koblede hendelsen."

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:94
msgid "Edge barrier minimum push"
msgstr "Kantgrensens minimumstrykk"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:103
msgid "Minimum push needed to overcome edge barrier."
msgstr "Minimumstrykk for å overkomme kantgrensen."

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:104
msgid ""
"How much you have to push (in grid units) the mouse against an edge barrier "
"when sensibility is 100."
msgstr ""
"Hvor mye du må trykke musen (i skjermenheter) mot kanten når følsomheten er "
"100."

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:146
msgid "Edge barrier maximum push"
msgstr "Kantgrense maksimumspress"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:155
msgid "Maximum push needed to overcome edge barrier."
msgstr "Maksimumspress for å oberkomme kantgrensen"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:156
msgid ""
"How much you have to push (in grid units) the mouse against an edge barrier "
"when sensibility is 1."
msgstr ""
"Hvor mye du må presse musen (i skjermenheter) mot en kantgrense når "
"følsomheten er 1."

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:198
msgid "Edge drag areas width"
msgstr "Kantflyttingsområde bredde"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:210
msgid "The width of the edge drag areas"
msgstr "Bredden av kantflyttingsområdet"

#: ../src/app/qml/behaviourTab/Unity8EdgeSensitivity.qml:211
msgid "How big (in grid units) the edge-drag sensitive area should be."
msgstr ""
"Hvor stort (i skjermenheter) kantflyttingsområdets følsomhetsområde burde "
"være."

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:49
msgid "Enable the indicator menu"
msgstr "Skru på indikatormenyen"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:50
msgid "Enable or disable the indicator menu"
msgstr "Skru på eller av indikatormenyen"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:65
msgid "Battery"
msgstr "Batteri"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:68
msgid "Show percentage on panel"
msgstr "Vis prosent på panelet"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:76
msgid "Date & Time"
msgstr "Dato og tid"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:79
#, fuzzy
msgid "Show Calendar"
msgstr "Vis kalender"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:88
msgid "Show Events"
msgstr "Vis hendelser"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:97
msgid "Show Week Numbers"
msgstr "Vis ukenummer"

#: ../src/app/qml/behaviourTab/Unity8Indicators.qml:98
msgid "Enable showing week numbers on calendar"
msgstr "Skru på visning av ukenummer i kalender"

#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:43
msgid "Enable the launcher"
msgstr "Skru på oppstarter"

#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:44
msgid "Enable or disable the launcher"
msgstr "Skru på eller av oppstarter"

#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:60
msgid "Autohide"
msgstr "Skjul automatisk"

#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:61
msgid ""
"This will only be applied in windowed mode. In staged mode, the launcher "
"will always hide."
msgstr ""
"Dette er kun for vindusmodus. I mobilmodus vil oppstarteren alltid skjules."

#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:72
msgid "Launcher width"
msgstr "Oppstarterbredde"

#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:81
msgid "Width of the launcher in grid units."
msgstr "Oppstarterbredde i skjermenheter."

#: ../src/app/qml/behaviourTab/Unity8Launcher.qml:82
msgid "Changes the width of the launcher in all usage modes."
msgstr "Endrer bredden av oppstarteren i alle modi."

#: ../src/app/qml/behaviourTab/Unity8Mode.qml:65
msgid ""
"This setting allows you to switch from the stage mode (default for mobile "
"device) to the windowed mode."
msgstr ""
"Denne innstillingen tillater deg å bytte mellom mobilmodus (vanlig for mobilenheter) "
"og vindusmodus."

#: ../src/app/qml/behaviourTab/Unity8Mode.qml:71
msgid "Staged"
msgstr "Mobilmodus"

#: ../src/app/qml/behaviourTab/Unity8Mode.qml:71
msgid "Windowed"
msgstr "Vindusmodus"

#: ../src/app/qml/behaviourTab/Unity8Mode.qml:71
msgid "Automatic"
msgstr "Automatisk"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:129
msgid "Large Text"
msgstr "Stor tekst"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:147
msgid "Normal Text"
msgstr "Normal tekst"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:157
msgid "Small Text"
msgstr "Liten tekst"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:172
msgid "Preview (Approximation only)"
msgstr "Forhåndsvisning (Kun tilnærmingsvis nøyaktig)"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:183
msgid "Grid Unit"
msgstr "Skjermenhet"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:191
#, fuzzy, qt-format
msgid "New value: %1"
msgstr "Gjeldende verdi: %1"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:192
#, fuzzy
msgid "Unity 8 scaling in grid units."
msgstr "Oppstarterbredde i skjermenheter."

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:193
msgid "How much pixels should there be in 1 grid unit."
msgstr "Ønsket størrelse på skjermenhet målt i piksler"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:234
msgid "Test"
msgstr "Prøv ut"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:244
msgid "Apply"
msgstr "Bruk"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:278
msgid ""
"This is just temporary and a device reboot will revert back to the default "
"scaling"
msgstr "Dette er kun midlertidig. Når enheten startes på nytt gjenopprettes "
"forvalgt skalering."

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:281
#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:292
msgid "This will make the scaling persistent and will survive a device reboot"
msgstr "Dette vil gjøre skaleringen vedvarende."

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:284
msgid "This will restore to the default scaling"
msgstr "Dette gjenoppretter forvalgt skalering"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:291
#, fuzzy
msgid "Apply new setting"
msgstr "Bruk den nye innstillingen"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:292
msgid ""
"This change requires restarting Unity 8 to take effect. Do you want to "
"restart Unity 8 now? All currently opened apps will be closed. Save all your "
"works before continuing!"
msgstr "Denne endringen krever omstart av Unity 8. Ønsker du omstart av "
"Unity 8 nå? Alle åpne apper vil bli lukket. Lagre alt arbeid før du "
"fortsetter!"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:293
msgid "Note:"
msgstr "Merk:"

#: ../src/app/qml/behaviourTab/Unity8Scaling.qml:301
msgid "Proceed"
msgstr "Fortsett"

#: ../src/app/qml/components/Dialogs/FileChooserDialog.qml:61
msgid "Select None"
msgstr "Velg ingen"

#: ../src/app/qml/components/Dialogs/FileChooserDialog.qml:63
msgid "Select All"
msgstr "Velg alle"

#: ../src/app/qml/components/Dialogs/FileChooserDialog.qml:201
msgid "Back to parent folder"
msgstr "Tilbake til mappe på nivået over"

#: ../src/app/qml/components/FilePickerPage.qml:31
msgid "Pick a file"
msgstr "Velg fil"

#: ../src/app/qml/main.qml:72
msgid "Behavior"
msgstr "Adferd"

#: ../src/app/qml/main.qml:78
msgid "Apps & Scopes"
msgstr "Apper og appvirkefelt"

#: ../src/app/qml/main.qml:84 ../src/app/qml/systemTab/SystemTab.qml:33
msgid "System"
msgstr "System"

#: ../src/app/qml/systemTab/ClickInstall.qml:32
#: ../src/app/qml/systemTab/ClickInstall.qml:146
#: ../src/app/qml/systemTab/SystemTab.qml:37
msgid "Install click package"
msgstr "Installer klikkpakke"

#: ../src/app/qml/systemTab/ClickInstall.qml:54
#, fuzzy
msgid "Select package"
msgstr "Velg pakke"

#: ../src/app/qml/systemTab/ClickInstall.qml:58
msgid "Selected package"
msgstr "Velg pakke"

#: ../src/app/qml/systemTab/ClickInstall.qml:59
msgid "<i>None</i>"
msgstr "<i>Ingen</i>"

#: ../src/app/qml/systemTab/ClickInstall.qml:63
msgid "Pick..."
msgstr "Velg..."

#: ../src/app/qml/systemTab/ClickInstall.qml:74
msgid "Install"
msgstr "Installer"

#: ../src/app/qml/systemTab/ClickInstall.qml:88
msgid "Output:"
msgstr "Resultat:"

#: ../src/app/qml/systemTab/ClickInstall.qml:115
msgid "Choose a package"
msgstr "Velg pakke"

#: ../src/app/qml/systemTab/ClickInstall.qml:131
msgid "Error"
msgstr "Feilmelding"

#: ../src/app/qml/systemTab/ClickInstall.qml:135
#: ../src/app/qml/systemTab/ClickInstall.qml:150
msgid "Close"
msgstr "Lukk"

#: ../src/app/qml/systemTab/ClickInstall.qml:147
msgid "The package has been successfully installed."
msgstr "Pakken er installert."

#: ../src/app/qml/systemTab/ImageWritable.qml:31
#: ../src/app/qml/systemTab/SystemTab.qml:42
msgid "Make image writable"
msgstr "Gjør systempakken skrivbar"

#: ../src/app/qml/systemTab/ImageWritable.qml:45
msgid ""
"Be very careful when editing the filesystem image. There is a high risk of "
"breaking OTA updates."
msgstr ""
"Vær veldig forsiktig når du redigerer filsystemets systempakke. Det er stor "
"risiko for å ødelegge for videre systemoppdateringer."

#: ../src/app/qml/systemTab/ImageWritable.qml:49
#: ../src/app/qml/systemTab/SSH.qml:55
msgid "Available settings"
msgstr "Tilgjengelige oppsett"

#: ../src/app/qml/systemTab/ImageWritable.qml:54
msgid "[Read-Only] Default setting"
msgstr "[Kun lesbar] Forvalgt innstilling"

#: ../src/app/qml/systemTab/ImageWritable.qml:55
msgid "[Read-Write] Temporary (until reboot)"
msgstr "[Les- og skrivbar] Midlertidig (til omstart)"

#: ../src/app/qml/systemTab/ImageWritable.qml:56
msgid "[Read-Write] Permanent (persists after reboot)"
msgstr "[Les- og skrivbar] Vedvarende (også etter omstart)"

#: ../src/app/qml/systemTab/ImageWritable.qml:108
#, fuzzy
msgid "Output"
msgstr "Resultat"

#: ../src/app/qml/systemTab/ImageWritable.qml:117
msgid "Any errors will be displayed below."
msgstr "Eventuelle feilmeldinger vises nedenfor."

#: ../src/app/qml/systemTab/SSH.qml:31
#: ../src/app/qml/systemTab/SystemTab.qml:48
#, fuzzy
msgid "SSH settings"
msgstr "SSH-innstillinger"

#: ../src/app/qml/systemTab/SSH.qml:45
msgid ""
"For information on how to configure SSH, refer back to the UBports "
"documentation:"
msgstr "For info om hvordan sette opp SSH, se UBports-dokumentasjonen:"

#. TRANSLATORS: Select the best available language for http://docs.ubports.com
#: ../src/app/qml/systemTab/SSH.qml:48
msgid "en"
msgstr "en"

#. TRANSLATORS: Use the documentation's page title for the selected language
#: ../src/app/qml/systemTab/SSH.qml:51
msgid "Shell access via ssh"
msgstr "Skalltilgang via SSH"

#: ../src/app/qml/systemTab/SSH.qml:58
msgid "Enable SSH access"
msgstr "Opprett SSH-forbindelse"

#: ../src/app/qml/systemTab/Services.qml:30
#: ../src/app/qml/systemTab/SystemTab.qml:78
msgid "Services"
msgstr "Tjenester"

#: ../src/app/qml/systemTab/Services.qml:42
#, fuzzy
msgid "Restart services"
msgstr "Enhetsomstart"

#: ../src/app/qml/systemTab/Services.qml:48
msgid "Close all running apps and restart the home screen."
msgstr "Lukk alle kjørende apper og ta omstart fra hjemmeskjermen"

#: ../src/app/qml/systemTab/SystemInfo.qml:29
msgid "<i>Unknown</i>"
msgstr "<i>Ukjent</i>"

#: ../src/app/qml/systemTab/SystemInfo.qml:40
msgid "System information"
msgstr "Systeminfo"

#: ../src/app/qml/systemTab/SystemInfo.qml:54
msgid "Ubuntu Touch information"
msgstr "Ubuntu Touch-info"

#: ../src/app/qml/systemTab/SystemInfo.qml:58
msgid "Kernel:"
msgstr "Kjerne:"

#: ../src/app/qml/systemTab/SystemInfo.qml:63
msgid "System platform:"
msgstr "Systemplattform:"

#: ../src/app/qml/systemTab/SystemInfo.qml:68
msgid "Distro:"
msgstr "Utgivelse:"

#: ../src/app/qml/systemTab/SystemInfo.qml:73
#, fuzzy
msgid "Desktop environment:"
msgstr "Skrivebordsmiljø:"

#: ../src/app/qml/systemTab/SystemInfo.qml:80
msgid "Hardware information"
msgstr "Maskinvareinfo"

#: ../src/app/qml/systemTab/SystemInfo.qml:84
msgid "Device name:"
msgstr "Enhetsnavn:"

#: ../src/app/qml/systemTab/SystemInfo.qml:89
msgid "CPU:"
msgstr "Prosessor:"

#: ../src/app/qml/systemTab/SystemInfo.qml:94
msgid "Memory:"
msgstr "Minne:"

#: ../src/app/qml/systemTab/SystemTab.qml:54
#, fuzzy
msgid "System informations"
msgstr "Systeminfo"

#: ../src/app/qml/systemTab/SystemTab.qml:61
msgid "USB settings"
msgstr "USB-oppsett"

#: ../src/app/qml/systemTab/SystemTab.qml:66
#: ../src/app/qml/systemTab/UsbMode.qml:31
msgid "ADB settings"
msgstr "ADB-oppsett"

#: ../src/app/qml/systemTab/SystemTab.qml:74
msgid "Utilities"
msgstr "Verktøy"

#: ../src/app/qml/systemTab/UsbMode.qml:44
msgid "USB mode"
msgstr "USB modus"

#: ../src/app/qml/systemTab/UsbMode.qml:50
msgid "MTP - Media Transfer Protocol"
msgstr "MTP - Media-overføringsprotokoll"

#: ../src/app/qml/systemTab/UsbMode.qml:51
msgid "RNDIS - Remote Network Driver Interface Specification"
msgstr "RNDIS - Fjernnettverksdrivergrensesnitt-spesifikasjon"

#: ../src/app/qml/systemTab/UsbMode.qml:77
msgid "Debugging"
msgstr "Avlusing"

#: ../src/app/qml/systemTab/UsbMode.qml:83
#: ../src/app/qml/systemTab/UsbMode.qml:102
msgid "Revoke USB debugging authorizations"
msgstr "Tilbakekall USB-feilsøkingstillatelser"

#: ../src/app/qml/systemTab/UsbMode.qml:103
msgid ""
"Revoke access to USB debugging from all computers you've previously "
"authorized?"
msgstr ""
"Tilbakekall tilgang til USB-feilsøking fra alle datamaskiner du tidligere har "
"gitt tilgang?"

#: ../src/plugin/pamauthentication/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "Autentisering kreves"

#: ../src/plugin/pamauthentication/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr "Skriv inn tilgangskode eller tilgangsfrase:"

#: ../src/plugin/pamauthentication/AuthenticationDialog.qml:40
msgid "passcode or passphrase"
msgstr "tilgangskode eller tilgangsfrase"

#: ../src/plugin/pamauthentication/AuthenticationDialog.qml:50
#: ../src/plugin/pamauthentication/NotifyDialog.qml:25
msgid "Ok"
msgstr "OK"

#: ../src/plugin/pamauthentication/AuthenticationService.qml:59
msgid "Authentication failed"
msgstr "Autentisering mislyktes"

#: ../src/plugin/tweaktool/clickinstaller.cpp:67
msgid "Failed with miscellaneous internal error."
msgstr "Mislyktes med forskjellige inerne feil."

#: ../src/plugin/tweaktool/clickinstaller.cpp:70
msgid "Failed with syntax error, or failed to parse command."
msgstr "Mislyktes med syktaksfeil eller mislykket kommandoanalyse."

#: ../src/plugin/tweaktool/clickinstaller.cpp:73
msgid "Failed as a file or directory was not found."
msgstr "Mislykket da fil eller mappe ikke ble funnet."

#: ../src/plugin/tweaktool/clickinstaller.cpp:76
msgid "Nothing useful was done."
msgstr "Ingenting av nytte ble utført."

#: ../src/plugin/tweaktool/clickinstaller.cpp:79
msgid "The initial setup failed, e.g. setting the network proxy."
msgstr "Utgangsoppsettet feilet, f.eks. oppsett av nettverks proxy."

#: ../src/plugin/tweaktool/clickinstaller.cpp:82
msgid "The transaction failed, see the detailed error for more information."
msgstr "Overførsel mislyktes, les detaljert feilrapport for mer informasjon."

#~ msgid "Write permissions"
#~ msgstr "Skrivetillatelser"

#~ msgid "Reboot device"
#~ msgstr "Enhetsomstart"

#~ msgid ""
#~ "This change require a reboot to take effect. Do you want to reboot the "
#~ "device now?"
#~ msgstr ""
#~ "Denne forandringen krever en omstart av enheten. Vil du ta en omstart nå?"

#~ msgid "Battery information"
#~ msgstr "Batteriinformasjon"

#~ msgid "Level:"
#~ msgstr "Nivå:"

#~ msgid "Cycle count:"
#~ msgstr "Repetisjoner:"

#~ msgid "Maximum capacity:"
#~ msgstr "Maksimumskapasitet:"

#~ msgid "Remaining capacity:"
#~ msgstr "Gjenværende kapasitet:"

#~ msgid "Capacity:"
#~ msgstr "Kapasitet:"

#~ msgid "Health:"
#~ msgstr "Tilstand:"

#~ msgid "Bad"
#~ msgstr "Ikke bra"

#~ msgid "Temperature:"
#~ msgstr "Temperatur:"

#~ msgid "Advanced settings for your Ubuntu device"
#~ msgstr "Avansert oppsett for din Ubuntu-enhet"
